package sef.module3.activity;


import java.util.Arrays;

public class Person {

    String name;

    public Person(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        //Person[] person = new Person[]{new Person("normz"), new Person("rachel")};

        int[] arrayOfNumbers = new int[3];

        arrayOfNumbers[0] = 10;
        arrayOfNumbers[1] = 3;
        arrayOfNumbers[2] = 7;

        Arrays.sort(arrayOfNumbers);

        for (int i = 0; i < arrayOfNumbers.length; i++) {
            System.out.println(arrayOfNumbers[i]);
        }
    }

}