package com.playground;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Program3 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int numberOfInputs = in.nextInt();
        List<Integer> results = new ArrayList<Integer>();
        for (int i = 1; i <= numberOfInputs; i++) {
            int start = in.nextInt();
            int end = in.nextInt();
            int divisibleBy = in.nextInt();
            int numberOfIntegersDivisibleBy = 0;
            for (int j = start; j <= end; j++) {
                if (j % divisibleBy == 0) {
                    numberOfIntegersDivisibleBy++;
                }
            }
            results.add(numberOfIntegersDivisibleBy);
        }
        int i = 1;
        for (Integer result : results) {
            System.out.println("Case " + i + ": " + result);
            i++;
        }
    }
}
