package com.playground;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public class LayoutXmlCreator {

    public static void main(String[] args) {
        // screen width 480, 600, 720
        // text size text_generic_normal, text_generic_480, text_generic_600, text_generic_720


        //String screenWidth = "480";
        String[] screenWidthList = {"320", "480", "600", "720"};

        for (String screenWidth : screenWidthList) {
            createLayoutFilesForThisScreenWidth(screenWidth);
        }
    }

    private static void createLayoutFilesForThisScreenWidth(String screenWidth) {
        String layoutPath = "files/layout";
        String screenWidthPath = layoutPath + "/layout-sw" + screenWidth + "dp";
        new File(screenWidthPath).mkdirs();
        File genericFolder = new File(layoutPath + "/generic");
        for (File xmlFile : genericFolder.listFiles()) {
            System.out.println(xmlFile.getName());
            Document xmlDocument = convertXMLFileToXMLDocument(xmlFile.getPath());

            xmlDocument = updateTextSizes(xmlDocument, "TextView", screenWidth);
            xmlDocument = updateTextSizes(xmlDocument, "EditText", screenWidth);
            writeXmlDocumentToXmlFile(xmlDocument, screenWidthPath + "/" + xmlFile.getName());
        }
    }

    private static Document updateTextSizes(Document xmlDocument, String tagName, String screenWidth) {
        NodeList textViews = xmlDocument.getElementsByTagName(tagName);
        for (int idx = 0; idx < textViews.getLength(); idx++) {
            Node textSizeAttrib = textViews.item(idx).getAttributes().getNamedItem("android:textSize");
            if (textSizeAttrib != null) {
                String textSize = textSizeAttrib.getNodeValue();
                String newTextSize;
                if (textSize.startsWith("@dimen/text_generic")) {
                    newTextSize = "@dimen/text_generic_" + screenWidth;
                } else {
                    newTextSize = "@dimen/" + upsizeHardcodedTextSize(textSize);
                }
                System.out.println(textSize + " -> " + newTextSize);
                textSizeAttrib.setNodeValue(newTextSize);
            }
        }
        return xmlDocument;
    }

    private static String upsizeHardcodedTextSize(String currentSize) {
        List<String> hardcodedSizes = new ArrayList<String>();
        hardcodedSizes.add("twelve");
        hardcodedSizes.add("fourteen");
        hardcodedSizes.add("sixteen");
        hardcodedSizes.add("eighteen");
        hardcodedSizes.add("twenty");
        hardcodedSizes.add("twentytwo");
        hardcodedSizes.add("twentyfour");
        hardcodedSizes.add("twentysix");
        hardcodedSizes.add("twentyeight");
        hardcodedSizes.add("thirty");
        hardcodedSizes.add("thirtytwo");
        hardcodedSizes.add("thirtyfour");
        hardcodedSizes.add("thirtysix");
        hardcodedSizes.add("thirtyeight");
        hardcodedSizes.add("forty");

        int upsize = 0;
        for (int i = 0; i < hardcodedSizes.size(); i++) {
            if (currentSize.endsWith(hardcodedSizes.get(i))) {
                if (i < hardcodedSizes.size() - 1) {
                    upsize = i + 1;
                    break;
                }
            }
        }
        return hardcodedSizes.get(upsize);
    }

    private static Document convertXMLFileToXMLDocument(String filePath)
    {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //API to obtain DOM Document instance
        DocumentBuilder builder;
        try
        {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();

            //Parse the content to Document object

            return builder.parse(new File(filePath));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private static void writeXmlDocumentToXmlFile(Document xmlDocument, String fileName)
    {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = tf.newTransformer();

            // Uncomment if you do not require XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            //Print XML or Logs or Console
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(xmlDocument), new StreamResult(writer));
            String xmlString = writer.getBuffer().toString();
            System.out.println(xmlString);

            //Write XML to file
            FileOutputStream outStream = new FileOutputStream(new File(fileName));
            transformer.transform(new DOMSource(xmlDocument), new StreamResult(outStream));
        }
        catch (TransformerException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
