package com.playground;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PaymentsCalculator {

    /**
     * Scenarios
     *** 1 term
     <=3000 = 0.08
     <=7000 = 0.06
     <=100000 = 0.05

     *** 2 terms
     <=3000 = 0.08
     <=7000 = 0.06
     <=100000 = 0.05

     *** 3 terms
     <=3000 = 0.08
     <=7000 = 0.06
     <=30000 = 0.05
     <=100000 = 0.04

     *** 4 terms
     <=3000 = 0.08
     <=7000 = 0.06
     <=30000 = 0.05
     <=100000 = 0.04
     */
    private static Calendar calendar;

    private static List<Payment> compute(double loanAmount, int daysToPay, Date cashOutDate) throws ParseException {

        calendar.setTime(cashOutDate);

        int terms = daysToPay / 15;
        double interest;
        if (loanAmount <= 3000) {
            interest = 0.08;
        } else if (loanAmount <= 7000) {
            interest = 0.06;
        } else if ((loanAmount <= 100000 && terms <= 2) || (loanAmount <= 30000 && terms >= 3)) {
            interest = 0.05;
        } else {
            interest = 0.04;
        }

        List<Payment> paymentList = new ArrayList<Payment>();
        for (int i = 0; i < terms; i++) {
            double amountToPay = (loanAmount / terms) + (loanAmount * interest / terms);
            int term = i + 1;
            calendar.add(Calendar.DATE, 15);
            Date dueDate = calendar.getTime();
            Payment payment = new Payment(term, amountToPay, dueDate);
            paymentList.add(payment);
        }

        return paymentList;
    }

    public static void main(String[] args) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        calendar = Calendar.getInstance();

        double loanAmount = 90000;
        int daysToPay = 45;
        Date cashOutDate = sdf.parse("2018-06-30");
        double totalAmountToPay = 0.0;
        try {
            List<Payment> paymentList = compute(loanAmount, daysToPay, cashOutDate);
            System.out.println("Loan amount: " + loanAmount);
            System.out.println("Days to pay: " + daysToPay);
            for (Payment payment : paymentList) {
                System.out.println(payment.getTerm() + " | " + sdf.format(payment.getDueDate()) + " | " + payment.getAmountToPay());
                totalAmountToPay += payment.getAmountToPay();
            }
            System.out.println("Total amount to pay: " + totalAmountToPay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}

class Payment {
    private int term;
    private double amountToPay;
    private Date dueDate;

    int getTerm() {
        return term;
    }

    double getAmountToPay() {
        return amountToPay;
    }

    Date getDueDate() {
        return dueDate;
    }

    Payment(int term, double amountToPay, Date dueDate) {
        this.term = term;
        this.amountToPay = amountToPay;
        this.dueDate = dueDate;
    }
}
