package com.playground;

import java.util.*;

public class TerritoryMap {

    static Scanner in;
    static char currentFaction;
    static int totalRows;
    static int totalCols;
    static char[][] map;
    static int currentRow;
    static int currentCol;

    static int contested;
    static Map<Character, Integer> factions = new HashMap<Character, Integer>();
    static List<Region> regions;

    public static void main(String[] args) {
        in = new Scanner(System.in);
        totalRows = in.nextInt();
        totalCols = in.nextInt();
        map = new char[totalRows][totalCols];
        in.nextLine();

        regions = new ArrayList<Region>();
        createMap();

        startFactionSearch();
    }

    static void createMap() {
        for (int i = 0; i < totalRows; i++) {
            map[i] = in.nextLine().toCharArray();
        }
    }

    static void startFactionSearch() {

        for (currentRow = 0; currentRow < totalRows; currentRow++) {
            List<RowLand> rowLands = extractRowLands();

            for (RowLand rowLand : rowLands) {
                findRegionForThisRowLand(rowLand);
            }
        }

        int regionCtr = 1;
        for (Region region : regions) {
            System.out.println("REGION " + regionCtr + " -----------------------------------");
            List<RowLand> rowLands = region.getRowLands();
            for (RowLand rowLand : rowLands) {
                System.out.println(rowLand.getLeftIndex() + " - " + rowLand.getRightIndex());
            }
            regionCtr++;
        }

    }

    static void findRegionForThisRowLand(RowLand rowLandToSearch) {
        boolean regionFound = false;
        for (Region region : regions) {
            List<RowLand> regionRowLands = region.getRowLands();
            RowLand lastRowLand = regionRowLands.get(regionRowLands.size()-1);
            int li1 = lastRowLand.getLeftIndex();
            int ri1 = lastRowLand.getRightIndex();
            int li2 = rowLandToSearch.getLeftIndex();
            int ri2 = rowLandToSearch.getRightIndex();
            if (li1 <= ri2 && ri1 <= li2)  {
                region.getRowLands().add(rowLandToSearch);
                regionFound = true;
            }
        }
        if (!regionFound) {
            Region region = new Region();
            region.getRowLands().add(rowLandToSearch);
            regions.add(region);
        }
    }

    static List<RowLand> extractRowLands() {
        int leftIndex = -1;
        List<RowLand> rowLands = new ArrayList<RowLand>();
        for (int i = 0; i < totalCols; i++) {
            char currentChar = map[currentRow][i];
            // if this is a land
            if (currentChar != '#') {
                // if this is a first land
                if (leftIndex == -1) {
                    leftIndex = i;
                }
                // if this is the last char on the row
                if (i == totalCols-1) {
                    // add row land, rightIndex is also the current one
                    rowLands.add(new RowLand(leftIndex, i));
                    leftIndex = -1;
                }
            } else { // if this is not a land
                if (leftIndex != -1) {
                    // add row land, rightIndex is the index before current one
                    rowLands.add(new RowLand(leftIndex, i-1));
                    leftIndex = -1;
                }
            }
        }

        for (RowLand rowLand : rowLands) {
            System.out.println(rowLand.getLeftIndex() + " - " + rowLand.getRightIndex());
        }
        return rowLands;
    }
}

class RowLand {

    private int leftIndex;
    private int rightIndex;

    public RowLand(int leftIndex, int rightIndex) {
        this.leftIndex = leftIndex;
        this.rightIndex = rightIndex;
    }

    public int getLeftIndex() {
        return leftIndex;
    }

    public void setLeftIndex(int leftIndex) {
        this.leftIndex = leftIndex;
    }

    public int getRightIndex() {
        return rightIndex;
    }

    public void setRightIndex(int rightIndex) {
        this.rightIndex = rightIndex;
    }
}

class Region {

    private List<RowLand> rowLands;
    private List<Point> points;

    public Region() {
        points = new ArrayList<Point>();
        rowLands = new ArrayList<RowLand>();
    }

    public List<RowLand> getRowLands() {
        return rowLands;
    }

    public void setRowLands(List<RowLand> rowLands) {
        this.rowLands = rowLands;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }
}

class Point {

    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}