package com.playground;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class WordSearch {
    static int numberOfRows;
    static int numberOfColumns;
    static char[][] grid;
    static String wordToSearch;
    static int currentRow;
    static int currentColumn;
    static Scanner in = new Scanner(System.in);
    static List<Integer> results = new ArrayList<Integer>();

    public static void main(String[] args) {
        int totalNumberOfGrids = in.nextInt();

        for (int x = 0; x < totalNumberOfGrids; x++) {
            numberOfRows = in.nextInt();
            numberOfColumns = in.nextInt();
            grid = new char[numberOfRows][];
            in.nextLine();
            createGrid();
            wordToSearch = in.nextLine().toLowerCase();
            startWordSearch();
        }

        int resultCtr = 1;
        for (Integer result : results) {
            System.out.println("Case " + resultCtr + ": " + result);
            resultCtr++;
        }
    }

    public static void createGrid() {
        for (int i = 0; i < numberOfRows; i++) {
            grid[i] = in.nextLine().toLowerCase().toCharArray();
        }
    }

    public static void startWordSearch() {
        //System.out.println("Search me: " + wordToSearch);
        //System.out.println("word length: " + wordToSearch.length());
        int totalMatches = 0;

        for (currentRow = 0; currentRow < numberOfRows; currentRow++) {
            for (currentColumn = 0; currentColumn < numberOfColumns; currentColumn++) {
                if (grid[currentRow][currentColumn] == wordToSearch.charAt(0)) {
                    //System.out.println("grid[" + currentRow + "][" + currentColumn + "]");
                    if (findRightward()) {
                        totalMatches++;
                        //System.out.println("Match found at ROW " + currentRow + " COL " + currentColumn);
                    }
                    if (findLeftward()) {
                        totalMatches++;
                        //System.out.println("Match found at ROW " + currentRow + " COL " + currentColumn);
                    }
                    if (findDownward()) {
                        totalMatches++;
                        //System.out.println("Match found at ROW " + currentRow + " COL " + currentColumn);
                    }
                    if (findUpward()) {
                        totalMatches++;
                        //System.out.println("Match found at ROW " + currentRow + " COL " + currentColumn);
                    }
                    if (findDiagonalUpRight()) {
                        totalMatches++;
                        //System.out.println("Match found at ROW " + currentRow + " COL " + currentColumn);
                    }
                    if (findDiagonalUpLeft()) {
                        totalMatches++;
                        //System.out.println("Match found at ROW " + currentRow + " COL " + currentColumn);
                    }
                    if (findDiagonalDownRight()) {
                        totalMatches++;
                        //System.out.println("Match found at ROW " + currentRow + " COL " + currentColumn);
                    }
                    if (findDiagonalDownLeft()) {
                        totalMatches++;
                        //System.out.println("Match found at ROW " + currentRow + " COL " + currentColumn);
                    }
                }
            }
        }

        //System.out.println("Total matches in the grid: " + totalMatches);
        results.add(totalMatches);
    }

    public static boolean findRightward() {
        if (doesWordFitRightward()) {
            //System.out.println("findRightward");
            // start on current column moving rightward/forward
            StringBuilder sb = new StringBuilder();
            for (int i = currentColumn; i < currentColumn + wordToSearch.length(); i++) {
                sb.append(grid[currentRow][i]);
            }
            return sb.toString().equals(wordToSearch);
        }
        //System.out.println("doesn't fit");
        return false;
    }

    public static boolean findLeftward() {
        if (doesWordFitLeftward()) {
            //System.out.println("findLeftward");
            StringBuilder sb = new StringBuilder();
            int colIndex = currentColumn;
            for (int i = 0; i < wordToSearch.length(); i++) {
                sb.append(grid[currentRow][colIndex]);
                colIndex--;
            }
            return sb.toString().equals(wordToSearch);
        }
        //System.out.println("doesn't fit");
        return false;
    }

    public static boolean findDownward() {
        if (doesWordFitDownward()) {
            //System.out.println("findDownward");
            // start on current row moving downward
            StringBuilder sb = new StringBuilder();
            for (int i = currentRow; i < currentRow + wordToSearch.length(); i++) {
                sb.append(grid[i][currentColumn]);
            }
            return sb.toString().equals(wordToSearch);
        }
        //System.out.println("doesn't fit");
        return false;
    }

    public static boolean findUpward() {
        if (doesWordFitUpward()) {
            //System.out.println("findUpward");
            // start on current row moving upward
            StringBuilder sb = new StringBuilder();
            int rowIndex = currentRow;
            for (int i = 0; i < wordToSearch.length(); i++) {
                sb.append(grid[rowIndex][currentColumn]);
                rowIndex--;
            }
            return sb.toString().equals(wordToSearch);
        }
        //System.out.println("doesn't fit");
        return false;
    }

    public static boolean findDiagonalUpRight() {
        if (doesWordFitUpward() && doesWordFitRightward()) {
            //System.out.println("findDiagonalUpRight");
            StringBuilder sb = new StringBuilder();
            int rowIndex = currentRow;
            int colIndex = currentColumn;
            for (int i = 0; i < wordToSearch.length(); i++) {
                sb.append(grid[rowIndex][colIndex]);
                rowIndex--;
                colIndex++;
            }
            return sb.toString().equals(wordToSearch);
        }
        //System.out.println("doesn't fit");
        return false;
    }

    public static boolean findDiagonalUpLeft() {
        if (doesWordFitUpward() && doesWordFitLeftward()) {
            //System.out.println("findDiagonalUpLeft");
            StringBuilder sb = new StringBuilder();
            int rowIndex = currentRow;
            int colIndex = currentColumn;
            for (int i = 0; i < wordToSearch.length(); i++) {
                sb.append(grid[rowIndex][colIndex]);
                rowIndex--;
                colIndex--;
            }
            return sb.toString().equals(wordToSearch);
        }
        //System.out.println("doesn't fit");
        return false;
    }

    public static boolean findDiagonalDownRight() {
        if (doesWordFitDownward() && doesWordFitRightward()) {
            //System.out.println("findDiagonalDownRight");
            StringBuilder sb = new StringBuilder();
            int rowIndex = currentRow;
            int colIndex = currentColumn;
            for (int i = 0; i < wordToSearch.length(); i++) {
                sb.append(grid[rowIndex][colIndex]);
                rowIndex++;
                colIndex++;
            }
            return sb.toString().equals(wordToSearch);
        }
        //System.out.println("doesn't fit");
        return false;
    }

    public static boolean findDiagonalDownLeft() {
        if (doesWordFitDownward() && doesWordFitLeftward()) {
            //System.out.println("findDiagonalDownLeft");
            StringBuilder sb = new StringBuilder();
            int rowIndex = currentRow;
            int colIndex = currentColumn;
            for (int i = 0; i < wordToSearch.length(); i++) {
                sb.append(grid[rowIndex][colIndex]);
                rowIndex++;
                colIndex--;
            }
            return sb.toString().equals(wordToSearch);
        }
        //System.out.println("doesn't fit");
        return false;
    }

    public static boolean doesWordFitRightward() {
        return currentColumn + wordToSearch.length() <= numberOfColumns;
    }

    public static boolean doesWordFitLeftward() {
        return currentColumn+1 - wordToSearch.length() >= 0;
    }

    public static boolean doesWordFitUpward() {
        return currentRow+1 - wordToSearch.length() >= 0;
    }

    public static boolean doesWordFitDownward() {
        return currentRow + wordToSearch.length() <= numberOfRows;
    }
}
