package com.playground;

import java.io.File;
import java.io.FilenameFilter;

public class FileRenamer {

    public static void main(String[] args) {

        // [HorribleSubs] Naruto Shippuuden - 378 [720p].mkv
        // [AnimeRG] Naruto Shippuden - 387 [1080p] [x265] [pseudo]
        // [AnimeRG] Naruto Shippuden - 394 [1080p] [Multi-Sub] [x265] [pseudo].mkv

        String path = "D:\\Videos\\Anime\\Naruto Shippuuden\\";
        String prefixToReplace = "[AnimeRG] ";
        String suffixToReplace = " [1080p] [Multi-Sub] [x265] [pseudo]";

        File dir = new File(path);
        File [] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith("[AnimeRG]");
            }
        });

        for (File oldFile : files) {
            String filename = oldFile.getName();
            String newFilename = filename.replace(prefixToReplace, "");
            newFilename = newFilename.replace(suffixToReplace, "");
            File newFile = new File(path + newFilename);
            oldFile.renameTo(newFile);
            System.out.println(oldFile + " -> " + newFile);
        }
    }
}
