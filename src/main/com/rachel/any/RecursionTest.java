package com.rachel.any;

public class RecursionTest {

    // Rachel
    static int add(int num) {
        int n1 = num % 10;
        int n2;

        if (num < 10) {
            return n1;
        } else {
            n2 = num / 10;
            return n1 + add(n2);
        }
    }

    public static void main(String[] args) {
        int num = 123;
        System.out.println(add(num));
        System.out.println(add2(num));
    }

    // Normz
    static int add2(int num) {
        int sum = 0;
        int newNum = num / 10;
        if (newNum != 0) {
            int fnum = num % 10;
            sum = sum + fnum;
            return sum + add2(newNum);
        } else {
            return num;
        }
    }
}
