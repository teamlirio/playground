package com.rachel.any;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rachel on 3/4/2016.
 */
public class ListTest {

    static List<Note> noteList;

    public static void main(String[] args) {

        noteList = new ArrayList<Note>();

        Note note1 = new Note();
        note1.setText("note 1");
        note1.getNames().add("rachel");
        note1.getNames().add("normz");
        noteList.add(note1);

        anotherMethod();
        anotherMethodAgain();

        for (Note note: noteList) {
            System.out.println(note.getText());
            for (String name : note.getNames()) {
                System.out.println(" -" + name);
            }
        }

    }

    static void anotherMethod() {
        Note note = new Note();
        note.setText("anotherMethod note");
        note.getNames().add("anotherMethod name1");
        note.getNames().add("anotherMethod name2");
        noteList.add(note);
    }

    static void anotherMethodAgain() {
        for (Note note : noteList) {
            note.getNames().add("third name");
        }
    }
}

class Note {
    private String text;
    private List<String> names;

    public Note() {
        names = new ArrayList<String>();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }
}
