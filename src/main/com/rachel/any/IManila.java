package com.rachel.any;

/**
 * Created by rachel on 5/27/2016.
 */
public class IManila {

    public static int compute(int num) {
        if (num == 1) {
            return num;
        }
        return num * compute(num-1);
    }

    public static void main(String[] args) {
        System.out.println(compute(4));
    }
}
