package com.rachel.any;



import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rachel on 1/16/2017.
 */
public class JsonTest {

    public static void main(String[] args) {
        test2();
    }

    class Useracct {
        String acctid;
        String ufname;
        String umname;
        String ulname;
        String role;

        public String getAcctid() {
            return acctid;
        }

        public void setAcctid(String acctid) {
            this.acctid = acctid;
        }

        public String getUfname() {
            return ufname;
        }

        public void setUfname(String ufname) {
            this.ufname = ufname;
        }

        public String getUmname() {
            return umname;
        }

        public void setUmname(String umname) {
            this.umname = umname;
        }

        public String getUlname() {
            return ulname;
        }

        public void setUlname(String ulname) {
            this.ulname = ulname;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }
    }

    class Storelist {
        String storeid;
        String storename;
        String storeadd;
        String latitude;
        String longitude;
        String clustertype;
        String contactperson;
        String contactjobtitle;
        String dob;
        String acctid;
        boolean storeflag;

        public String getStoreid() {
            return storeid;
        }

        public void setStoreid(String storeid) {
            this.storeid = storeid;
        }

        public String getStorename() {
            return storename;
        }

        public void setStorename(String storename) {
            this.storename = storename;
        }

        public String getStoreadd() {
            return storeadd;
        }

        public void setStoreadd(String storeadd) {
            this.storeadd = storeadd;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getClustertype() {
            return clustertype;
        }

        public void setClustertype(String clustertype) {
            this.clustertype = clustertype;
        }

        public String getContactperson() {
            return contactperson;
        }

        public void setContactperson(String contactperson) {
            this.contactperson = contactperson;
        }

        public String getContactjobtitle() {
            return contactjobtitle;
        }

        public void setContactjobtitle(String contactjobtitle) {
            this.contactjobtitle = contactjobtitle;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getAcctid() {
            return acctid;
        }

        public void setAcctid(String acctid) {
            this.acctid = acctid;
        }

        public boolean isStoreflag() {
            return storeflag;
        }

        public void setStoreflag(boolean storeflag) {
            this.storeflag = storeflag;
        }
    }

    class Data {
        List<Useracct> useracctList;
        List<Storelist> storeList;

        public List<Useracct> getUseracctList() {
            return useracctList;
        }

        public void setUseracctList(List<Useracct> useracctList) {
            this.useracctList = useracctList;
        }

        public List<Storelist> getStoreList() {
            return storeList;
        }

        public void setStoreList(List<Storelist> storeList) {
            this.storeList = storeList;
        }
    }

    class FullResponse {
        String response;
        List<Data> dataList;

        public String getResponse() {
            return response;
        }

        public void setResponse(String response) {
            this.response = response;
        }

        public List<Data> getDataList() {
            return dataList;
        }

        public void setDataList(List<Data> dataList) {
            this.dataList = dataList;
        }
    }

    private static void test2() {

        String storeResponse = "[  \n" +
                "   {  \n" +
                "      \"response\":\"success\",\n" +
                "      \"data\":[\n" +
                "         {  \n" +
                "            \"useracct\":[\n" +
                "               {  \n" +
                "                  \"acctid\":\"3\",\n" +
                "                  \"ufname\":\"Norman\",\n" +
                "                  \"umname\":\"\",\n" +
                "                  \"ulname\":\"\",\n" +
                "                  \"role\":\"0\"\n" +
                "               }\n" +
                "            ],\n" +
                "            \"storelist\":[\n" +
                "               {  \n" +
                "                  \"storeid\":\"2\",\n" +
                "                  \"storename\":\"7-Eleven Baler Project 7\",\n" +
                "                  \"storecode\":\"002\",\n" +
                "                  \"storeadd\":\"89 Baler, Project 7, Quezon City, 1105 Metro Manila\",\n" +
                "                  \"latitude\":\"14.648189\",\n" +
                "                  \"longitude\":\"121.027433\",\n" +
                "                  \"clustertype\":\"Residential\",\n" +
                "                  \"contactperson\":\"Limorda Sialab\",\n" +
                "                  \"contactjobtitle\":\"Store Manager\",\n" +
                "                  \"dob\":\"0000-00-00\",\n" +
                "                  \"acctid\":\"3\",\n" +
                "                  \"storeflag\":\"false\"\n" +
                "               }\n" +
                "            ]\n" +
                "         }\n" +
                "      ]\n" +
                "   }\n" +
                "]";


        try {
            List<JSONObject> rootList = convertToList(new JSONArray(storeResponse));
            for (JSONObject item : rootList) {
                System.out.println(item.getString("response"));
                List<JSONObject> dataList = convertToList(item.getJSONArray("data"));
                for (JSONObject data: dataList) {
                    List<JSONObject> useracctList = convertToList(data.getJSONArray("useracct"));
                    for (JSONObject useracct : useracctList) {
                        System.out.println("acctid: " + useracct.getString("acctid"));
                        System.out.println("ufname: " + useracct.getString("ufname"));
                    }
                    List<JSONObject> storeList = convertToList(data.getJSONArray("storelist"));
                    for (JSONObject store: storeList) {
                        System.out.println("storeid: " + store.getString("storeid"));
                        System.out.println("storename: " + store.getString("storename"));
                    }
                }
            }
        } catch (JSONException e) {
            System.out.println("JSONException encountered.");
            e.printStackTrace();
        }
    }

    public static List<JSONObject> convertToList(JSONArray jsonArray) {
        List<JSONObject> list = new ArrayList<JSONObject>();
        for(int i = 0; i < jsonArray.length(); i++){
            try {
                list.add(jsonArray.getJSONObject(i));
            } catch (JSONException e) {
                System.out.println("Unable to convert to list");
                e.printStackTrace();
            }
        }
        return list;
    }

    public void test1() {
        String yourJSONresponse = "[\n" +
                "\"A00.0 : CHOLERA CHOLERA DUE TO VIBRIO CHOLERAE 01, BIOVAR CHOLERAE:NON - DREADED\",\n" +
                "\"A00.1 : CHOLERA CHOLERA DUE TO VIBRIO CHOLERAE 01, BIOVAR ELTOR:NON - DREADED\",\n" +
                "\"A00.9 : CHOLERA CHOLERA, UNSPECIFIED:DREADED\",\n" +
                "\"A01.0 : TYPHOID AND PARATYPHOID FEVERS TYPHOID FEVER:NON - DREADED\",\n" +
                "\"A01.1 : TYPHOID AND PARATYPHOID FEVERS PARATYPHOID FEVER A:NON - DREADED\",\n" +
                "\"A01.2 : TYPHOID AND PARATYPHOID FEVERS PARATYPHOID FEVER B:NON - DREADED\",\n" +
                "\"A01.3 : TYPHOID AND PARATYPHOID FEVERS PARATYPHOID FEVER C:NON - DREADED\",\n" +
                "\"A01.4 : TYPHOID AND PARATYPHOID FEVERS PARATYPHOID FEVER, UNSPECIFIED:NON - DREADED\",\n" +
                "\"A02.0 : OTHER SALMONELLA INFECTIONS SALMONELLA ENTERITIS:NON - DREADED\",\n" +
                "\"A02.1 : OTHER SALMONELLA INFECTIONS SALMONELLA SEPTICAEMIA:NON - DREADED\",\n" +
                "\"A02.2 : OTHER SALMONELLA INFECTIONS LOCALIZED SALMONELLA INFECTIONS:NON - DREADED\",\n" +
                "\"A02.8 : OTHER SALMONELLA INFECTIONS OTHER SPECIFIED SALMONELLA INFECTIONS:NON - DREADED\",\n" +
                "\"A02.9 : OTHER SALMONELLA INFECTIONS SALMONELLA INFECTION, UNSPECIFIED:NON - DREADED\",\n" +
                "\"A03.0 : SHIGELLOSIS SHIGELLOSIS DUE TO SHIGELLA DYSENTERIAE:NON - DREADED\",\n" +
                "\"A03.1 : SHIGELLOSIS SHIGELLOSIS DUE TO SHIGELLA FLEXNERI:NON - DREADED\",\n" +
                "\"A03.2 : SHIGELLOSIS SHIGELLOSIS DUE TO SHIGELLA BOYDII:NON - DREADED\",\n" +
                "\"A03.3 : SHIGELLOSIS SHIGELLOSIS DUE TO SHIGELLA SONNEI:NON - DREADED\",\n" +
                "\"A03.8 : SHIGELLOSIS OTHER SHIGELLOSIS:NON - DREADED\",\n" +
                "\"A03.9 : SHIGELLOSIS SHIGELLOSIS, UNSPECIFIED:NON - DREADED\",\n" +
                "\"A04.0 : OTHER BACTERIAL INTESTINAL INFECTIONS ENTEROPATHOGENIC ESCHERICHIA COLI INFECTION:NON - DREADED\",\n" +
                "\"A04.1 : OTHER BACTERIAL INTESTINAL INFECTIONS ENTEROTOXIGENIC ESCHERICHIA COLI INFECTION:NON - DREADED\",\n" +
                "\"A04.2 : OTHER BACTERIAL INTESTINAL INFECTIONS ENTEROINVASIVE ESCHERICHIA COLI INFECTION:NON - DREADED\",\n" +
                "\"A04.3 : OTHER BACTERIAL INTESTINAL INFECTIONS ENTEROHAEMORRHAGIC ESCHERICHIA COLI INFECTION:NON - DREADED\",\n" +
                "\"A04.4 : OTHER BACTERIAL INTESTINAL INFECTIONS OTHER INTESTINAL ESCHERICHIA COLI INFECTIONS:NON - DREADED\",\n" +
                "\"A04.5 : OTHER BACTERIAL INTESTINAL INFECTIONS CAMPYLOBACTER ENTERITIS:NON - DREADED\",\n" +
                "\"A04.6 : OTHER BACTERIAL INTESTINAL INFECTIONS ENTERITIS DUE TO YERSINIA ENTEROCOLITICA:NON - DREADED\",\n" +
                "\"A04.7 : OTHER BACTERIAL INTESTINAL INFECTIONS ENTEROCOLITIS DUE TO CLOSTRIDIUM DIFFICILE:NON - DREADED\",\n" +
                "\"A04.8 : OTHER BACTERIAL INTESTINAL INFECTIONS OTHER SPECIFIED BACTERIAL INTESTINAL INFECTIONS:NON - DREADED\",\n" +
                "\"A04.9 : OTHER BACTERIAL INTESTINAL INFECTIONS BACTERIAL INTESTINAL INFECTION, UNSPECIFIED:NON - DREADED\",\n" +
                "\"A05.0 : OTHER BACTERIAL FOODBORNE INTOXICATIONS FOODBORNE STAPHYLOCOCCAL INTOXICATION:NON - DREADED\",\n" +
                "\"A05.1 : OTHER BACTERIAL FOODBORNE INTOXICATIONS BOTULISM:NON - DREADED\",\n" +
                "\"A05.2 : OTHER BACTERIAL FOODBORNE INTOXICATIONS FOODBORNE CLOSTRIDIUM PERFRINGENS [CLOSTRIDIUM WELCHII] INTOXICATION:NON - DREADED\",\n" +
                "\"A05.3 : OTHER BACTERIAL FOODBORNE INTOXICATIONS FOODBORNE VIBRIO PARAHAEMOLYTICUS INTOXICATION:NON - DREADED\",\n" +
                "\"A05.4 : OTHER BACTERIAL FOODBORNE INTOXICATIONS FOODBORNE BACILLUS CEREUS INTOXICATION:NON - DREADED\",\n" +
                "\"A05.8 : OTHER BACTERIAL FOODBORNE INTOXICATIONS OTHER SPECIFIED BACTERIAL FOODBORNE INTOXICATIONS:NON - DREADED\",\n" +
                "\"A05.9 : OTHER BACTERIAL FOODBORNE INTOXICATIONS BACTERIAL FOODBORNE INTOXICATION, UNSPECIFIED:NON - DREADED\",\n" +
                "\"A06.0 : AMOEBIASIS ACUTE AMOEBIC DYSENTERY:NON - DREADED\",\n" +
                "\"A06.1 : AMOEBIASIS CHRONIC INTESTINAL AMOEBIASIS:NON - DREADED\",\n" +
                "\"A06.2 : AMOEBIASIS AMOEBIC NONDYSENTERIC COLITIS:NON - DREADED\",\n" +
                "\"A06.3 : AMOEBIASIS AMOEBOMA OF INTESTINE:NON - DREADED\",\n" +
                "\"A06.4 : AMOEBIASIS AMOEBIC LIVER ABSCESS:NON - DREADED\",\n" +
                "\"A06.5 : AMOEBIASIS AMOEBIC LUNG ABSCESS:NON - DREADED\",\n" +
                "\"A06.6 : AMOEBIASIS AMOEBIC BRAIN ABSCESS:NON - DREADED\",\n" +
                "\"A06.7 : AMOEBIASIS CUTANEOUS AMOEBIASIS:NON - DREADED\",\n" +
                "\"A06.8 : AMOEBIASIS AMOEBIC INFECTION OF OTHER SITES:NON - DREADED\",\n" +
                "\"A06.9 : AMOEBIASIS AMOEBIASIS, UNSPECIFIED:NON - DREADED\",\n" +
                "\"A07.0 : OTHER PROTOZOAL INTESTINAL DISEASES BALANTIDIASIS:NON - DREADED\",\n" +
                "\"A07.1 : OTHER PROTOZOAL INTESTINAL DISEASES GIARDIASIS [LAMBLIASIS]:NON - DREADED\",\n" +
                "\"A07.2 : OTHER PROTOZOAL INTESTINAL DISEASES CRYPTOSPORIDIOSIS:NON - DREADED\",\n" +
                "\"A07.3 : OTHER PROTOZOAL INTESTINAL DISEASES ISOSPORIASIS:NON - DREADED\"\n" +
                "]\n";

        List<String> list = new ArrayList<String>();
        try {
            JSONArray arr = new JSONArray(yourJSONresponse);
            for (int i = 0; i < arr.length(); i++) {
                list.add(arr.get(i).toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (String s : list) {
            System.out.println(s);
        }
    }
}
