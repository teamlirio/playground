package com.rachel.any;

import java.util.*;

/**
 * Created by rachel on 11/28/2016.
 */
public class MapTest {

    public static void main(String[] args) {
        List<String> listOfWords = new ArrayList<String>();
        listOfWords.addAll(Arrays.asList("Apple",
                "Adobe",
                "Aso",
                "Baboy",
                "Bilog",
                "Chupchup",
                "Cat",
                "Dog"));
        Map<Character, List<String>> output = new HashMap<Character, List<String>>();
        for (String word : listOfWords) {
            Character firstChar = word.charAt(0);
            List<String> currentList;
            if (output.containsKey(firstChar)) {
                currentList = output.get(firstChar);
            } else {
                currentList = new ArrayList<String>();
            }
            currentList.add(word);
            output.put(firstChar, currentList);
        }

        for (Character c : output.keySet()) {
            List<String> words = output.get(c);
            System.out.println(c);
            for (String s : words) {
                System.out.println(s);
            }
        }
    }
}
