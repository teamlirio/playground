package com.rachel.any;

/**
 * Created by rachel on 2/18/2016.
 */
public class Solution {

    static int solution(int M, int[] A) {
        int N = A.length;
        int[] count = new int[M + 1];
        for (int i = 0; i <= M; i++) {
            count[i] = 0;
        }
        int maxOccurence = 0; // MODIFIED
        int index = -1;
        for (int i = 0; i < N; i++) {
            if (count[A[i]] > 0) {
                int tmp = count[A[i]];
                if (tmp > maxOccurence) {
                    maxOccurence = tmp;
                    index = i;
                }
                count[A[i]] = tmp + 1;
            } else {
                count[A[i]] = 1;
            }
        }
        return A[index];
    }

    public static void main(String[] args) {
        int M = 3;
        int[] A = {1,3,2,1,2,1,5,3,3,4,2};
        int output = solution(M, A);
        System.out.println(output);
    }
}
