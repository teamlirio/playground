package com.rachel.any;

/**
 * Created by rachel on 2/19/2016.
 */
public class BuildingPainter {

    static int solution(int[] numbers) {
        int highestNumber = -1;

        // get highest number from the array
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > highestNumber) {
                highestNumber = numbers[i];
            }
        }

        int totalStrokes = 0;
        for (int level = 1; level <= highestNumber; level++) {
            boolean paint = false;
            int strokesPerLevel = 0;
            for (int index = 0; index < numbers.length; index++) {
                if (numbers[index] >= level) {
                    if (paint == false) {
                        strokesPerLevel++;
                        paint = true;
                    }
                } else {
                    paint = false;
                }

            }
            totalStrokes += strokesPerLevel;
        }

        return totalStrokes;
    }

    public static void main(String[] args) {
        System.out.println("Number of strokes: " + solution(new int[] {1,3,2,1,2,1,5,3,3,4,2}));
        System.out.println("Number of strokes: " + solution(new int[] {5,8}));
        System.out.println("Number of strokes: " + solution(new int[] {1,1,1,1}));
    }
}
