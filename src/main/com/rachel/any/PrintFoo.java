package com.rachel.any;

import java.math.BigDecimal;

/**
 * Created by rachel on 8/24/2015.
 */
public class PrintFoo {

    public static int foo(int[] x, int a, int b, int i, int j) {
        int k = j;
        int ct = 0;
        while (k > i-1) {
            if (x[k] <= b && !(x[k] <= a)) {
                ct = ct + 1;
            }
            k = k - 1;
        }
        return ct;
    }

    public static void main(String[] args) {
        int[] x = {11, 10, 10, 5, 10, 15, 20, 10, 7, 11};
        System.out.print(foo(x, 8, 18, 3, 6)); //2
        System.out.print(foo(x, 10, 20, 0, 9)); //4
        System.out.print(foo(x, 8, 18, 6, 3)); //0
        System.out.print(foo(x, 20, 10, 0, 9)); //0
        System.out.print(foo(x, 6, 7, 8, 8)); //1


        System.out.println();
        int[] y = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int m = y.length - 1;
        int i = 3;
        int ct = 0;
        System.out.println("first index:" + m);
        while (m > i - 1) {
            ct = ct + 1;
            m = m - 1;
        }
        System.out.println("last index:" + m);
        System.out.println("ct:" + ct);

        BigDecimal bd = new BigDecimal(1000000000);

    }

}
