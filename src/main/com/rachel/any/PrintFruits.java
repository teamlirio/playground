package com.rachel.any;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by rachel on 8/25/2015.
 */
public class PrintFruits {

    private static String g(String str) {
        int i = 0;
        String newStr = "";
        while (i < str.length() - 1) {
            newStr = newStr + str.charAt(i + 1);
            i = i + 1;
        }
        return newStr;
    }

    private static String f(String str) {
        if (str.length() == 0) {
            return "";
        } else if (str.length() == 1) {
            return str;
        } else {
            return f(g(str)) + str.charAt(0);
        }
    }

    private static String h(int n, String str) {
        while (n != 1) {
            if (n % 2 == 0) {
                n = n/2;
            } else {
                n = 3*n + 1;
            }
            str = f(str);
        }
        return str;
    }

    private static int pow(int x, int y) {
        if (y == 0) {
            return 1;
        } else {
            return x * pow(x, y-1);
        }
    }

    public static void main(String[] args) {
        System.out.print(h(1, "fruits"));
        System.out.print(h(2, "fruits"));
        System.out.print(h(5, "fruits"));
        System.out.print(h(pow(2, 10), "fruits"));
        System.out.print(h(pow(2, 17), "fruits"));
        System.out.println(0%3);



        /*Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int number = 1 + Math.abs(random.nextInt()) % 100;
        int userGuess;
        do {
            userGuess = scanner.nextInt();
            if (userGuess > number) {
                System.out.println("Too big...");
            } else if (userGuess < number) {
                System.out.println("Too small....");
            }
        } while (number != userGuess);*/


        Scanner userInput = new Scanner(System.in);
        int ui = 0;

        for(int x=1 ; ui >  -1; x++)
        {
            System.out.println("Enter test score: ");
            ui = userInput.nextInt();
            System.out.println("got it");

        }
    }
}