package com.rachel.any;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rachel on 9/9/2015.
 */
public class CaseCX {

    public class TestCaseX {
        public int a;
        public int b;

        public TestCaseX(int a, int b) {
            this.a = a;
            this.b = b;
        }
    }

    public class InputX {
        public int numberOfTestcases;
        List<TestCaseX> testCaseList = new ArrayList<TestCaseX>();

        public InputX(int numberOfTestcases, List<TestCaseX> testCaseList) {
            this.numberOfTestcases = numberOfTestcases;
            this.testCaseList = testCaseList;
        }
    }

    public void tryThis() {
        try {
            int numberOfTestcases = System.in.read();
            for (int i = 0; i < numberOfTestcases; i++) {
                int a = System.in.read();
                int b = System.in.read();
                TestCaseX testCase = new TestCaseX(a, b);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
