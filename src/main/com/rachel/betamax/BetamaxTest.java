package com.rachel.betamax;

import co.freeside.betamax.Betamax;
import co.freeside.betamax.Recorder;
import co.freeside.betamax.TapeMode;
import co.freeside.betamax.httpclient.BetamaxRoutePlanner;
import co.freeside.betamax.proxy.jetty.ProxyServer;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.*;

import java.io.IOException;

public class BetamaxTest {

    public static final String ACTIVE_STATUS = "http://54.187.104.23:8080/controller/wmm/security/details/activeStatus/ewise1";
    public static final String CARTRAWLER = "https://ota.cartrawler.com/cartrawlerota";

    @Rule
    public Recorder recorder = buildBetaMaxRecorder();

    private ProxyServer proxyServer = new ProxyServer(recorder);

    private Recorder buildBetaMaxRecorder() {
        Recorder recorder = new Recorder();
        //recorder.setSslSupport(true);
        return recorder;
    }

    private HttpResponse getHttpResponseUsingApacheDefaultClient(String url) throws IOException {
        DefaultHttpClient client = new DefaultHttpClient();
        BetamaxRoutePlanner.configure(client);
        HttpGet getMethod = new HttpGet(url);
        return client.execute(getMethod);
    }

    @Betamax(tape = "active-status",
            mode = TapeMode.WRITE_ONLY)
    @Test
    public void shouldRecordData() throws IOException {
        HttpResponse response = getHttpResponseUsingApacheDefaultClient(ACTIVE_STATUS);
        System.out.println(response.getStatusLine().getStatusCode());
    }

    /*@Betamax(tape = "cartrawler-response-recording",
            mode = TapeMode.WRITE_ONLY)
    @Test
    public void shouldRecordDataForCarTrawler() throws IOException, URISyntaxException {
        DefaultHttpClient client = new SystemDefaultHttpClient();
        HttpPost post = new HttpPost();
        post.setEntity(new StringEntity("", "UTF-8"));
        post.setURI(new URI(CARTRAWLER));
        BetamaxHttpsSupport.configure(client);
        HttpResponse response = client.execute(post);
        System.out.println(response.getStatusLine().getStatusCode());
    }

    @Test
    @Betamax(tape = "euro-exchange-response",
            mode = TapeMode.READ_ONLY)
    public void shouldReplayExchangeRateResponseWithDefaultHttpClient() throws Exception {
        HttpResponse response = getHttpResponseUsingApacheDefaultClient(ACTIVE_STATUS);
        System.out.println(response.getStatusLine().getStatusCode());
    }

    @Test
    @Betamax(tape = "euro-exchange-response",
            mode = TapeMode.READ_ONLY)
    public void shouldReplayResponseUsingSystemDefaultHttpClient() throws Exception {
        DefaultHttpClient client = new SystemDefaultHttpClient();
        HttpGet getMethod = new HttpGet(ACTIVE_STATUS);
        HttpResponse response = client.execute(getMethod);
        System.out.println(response.getStatusLine().getStatusCode());
    }*/


}
